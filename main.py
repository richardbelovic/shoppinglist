import cgi
import urllib
import wsgiref.handlers
import logging
from google.appengine.api import users
from google.appengine.ext import db
import webapp2

#Variable used for updating the numbers of each item
numOfItems = 0
sortByNum = True
#Class that sets up the database model, including the data in each record
class shopItem(db.Model):
	itemNum = db.IntegerProperty()
	name = db.StringProperty(multiline=True)
#Each different list for each user has a different key.
def list_key():
	#If there is a user logged in, then list key will be their user ID
	if users.get_current_user():
		return db.Key.from_path('ShoppingList', users.get_current_user().user_id())
	#No user = public list. This is not accessed since the user always has to be logged in
	else:
		return db.Key.from_path('ShoppingList', 'public_list')
#Method that checcks if something is an int
def isInt(i):
	#If there is no error, the value is an int, if there is, then it is not an int
	try:
		int(i)
		return True
	except ValueError:
		return False
#Gets the whole list of shopping items for the specific user.
def getQuery(sortByNumber = True):
	if sortByNumber:
		return db.GqlQuery("SELECT * "
						"FROM shopItem "
                        "WHERE ANCESTOR IS :1 "
						"ORDER BY itemNum ASC",
                        list_key())
	else:
		return db.GqlQuery("SELECT * "
						"FROM shopItem "
                        "WHERE ANCESTOR IS :1 "
						"ORDER BY name ASC",
                        list_key())
	

class MainPage(webapp2.RequestHandler):
    def get(self):
		#The webpage is hard coded into the python script.
		#response.out.write prints to the html of the web page.
		self.response.out.write('<html><body>')
		#gets numOfItems to use in the script
		global numOfItems
		#Checks if the user is logged in.
		if not users.get_current_user():
			self.redirect(users.create_login_url(self.request.uri))
		#Gets the whole list
		global sortByNum
		list = getQuery(sortByNum)
		numOfItems = 0
		#Goes through list, updating each record with the correct item number
		for item in list:
			numOfItems = numOfItems + 1
			item.itemNum = numOfItems
			item.put()
			#If there is an item number, then it prints the item number
			if item.itemNum:
				self.response.out.write(
				'%s: ' % item.itemNum)
		
			else:
				self.response.out.write('No item num')
			#print item name with a break afterwards
			self.response.out.write('%s<br>' % item.name)
		#Prints the rest of the page and all the buttons.
		self.response.out.write("""
          <form action="/add" method="post">
            <div><textarea name="name" rows="1" cols="30"></textarea></div>
            <div><input type="submit" value="Add item"></div>
          </form>
		  <form action="/sortByAlphabet" method="get">
		  	<div><input type="submit" value="Sort by alphabet"></div>
		  </form>
		  <form action="/deleteAll" method="get">
			<div><input type="submit" value="Delete all"></div>
		  </form>
		  <form action="/deleteOne" method="get">
			<div><textarea name="num" rows="1" cols="4"></textarea>Item number to delete</div>
			<div><textarea name="num2" rows="1" cols="4"></textarea>Second box for range</div>
			<div><input type="submit" value="Delete"></div>
		  </form>
		  <form action="/edit" method="get">
			<div><textarea name="editNum" rows="1" cols="4"></textarea>Item number to edit</div>
			<div><textarea name="nameChange" rows="1" cols="30"></textarea>Change to</div>
			<div><input type="submit" value="Edit"></div>
		  </form>
		  <form action="/logout" method="get">
			<div><input type="submit" value="Logout"></div>
		  </form>
        </body>
      </html>""")
#Class for adding item to list
class ShoppingList(webapp2.RequestHandler):
	def post(self):
		item = shopItem(parent=list_key())
		item.name = self.request.get('name')
		item.itemNum = numOfItems + 1;
		item.put()
		#Goes back to main page
		self.redirect('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()}))
#Deletes all the items in a user's list
class DeleteAll(webapp2.RequestHandler):
	def get(self):
		global sortByNum
		list = getQuery(sortByNum)
		for i in list:
			i.delete()
		self.redirect('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()}))
#Deletes one item or a range
class DeleteOne(webapp2.RequestHandler):
	def get(self):
		global sortByNum
		list = getQuery(sortByNum)
		#Checks if both the values are numbers to prevent errors. If they are not then they are set to 0
		if isInt(self.request.get('num')):
			num1 = int(self.request.get('num'))
		else:
			num1 = 0;
		if isInt(self.request.get('num2')):
			num2 = int(self.request.get('num2'))
		else:
			num2 = 0;
		#Does range delete if num2 is higher than num1
		if num2 > 0 and num1 > 0 and num2 > num1:
			for i in list:
				if int(i.itemNum) >= num1 and int(i.itemNum) <= num2:
					i.delete()
		elif num2 > 0 and num1 > 0 and num2 < num1:
			for i in list:
				if int(i.itemNum) <= num1 and int(i.itemNum0) >= num2:
					i.delete()
		#Does range delete if num1 is higher than num2
		else:
			for i in list:
				if int(i.itemNum) == int(num1):
					i.delete()
					break
		self.redirect('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()}))
#Edits a record
class Edit(webapp2.RequestHandler):
	def get(self):
		global sortByNum
		list = getQuery(sortByNum)
		#Checks for int to prevent errors
		if isInt(self.request.get("editNum")):
			editNum = int(self.request.get("editNum"))
		else:
			editNum = 0
		
		nameChange = self.request.get("nameChange")
		
		#goes through list and changes the record of the item with the right number
		for i in list:
			if i.itemNum == editNum:
				i.name = nameChange
				i.put()
				break
		self.redirect('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()}))
#Logs out the user
class Logout(webapp2.RequestHandler):
	def get(self):
		self.redirect(users.create_logout_url('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()})))
class SortByAlphabet(webapp2.RequestHandler):
	def get(self):
		global sortByNum
		sortByNum = False
		self.redirect('/?' + urllib.urlencode({'ShoppingList': users.get_current_user().user_id()}))
# (as far as I know) this sets up event handling. If an action is triggered on the webpage, it goes to a specific class.
application = webapp2.WSGIApplication([
	('/', MainPage),
	('/add',ShoppingList),
	('/deleteAll', DeleteAll),
	('/edit', Edit),
	('/logout', Logout),
	('/sortByAlphabet', SortByAlphabet),
	('/deleteOne', DeleteOne)
], debug=True)
#Begins application
def main():
    application.run()
#Calls main at the start
if __name__ == "__main__":
    main()
